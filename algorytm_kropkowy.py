import numpy as np
import matplotlib.pyplot as plt

def odczyt_pliku_fasta():
    """
        Funkcja umożliwia odczyt danych typu fasta,
        wprowadzenie sekwencji oraz wybór jednej z dwóch opcji.

    Returns:
        sekwencja1, sekwencja2
    """

    opcja = input('Wybierz:\n '
                   '1 - wprowadzić dane ręcznie\n'
                   '2 - wprowadzić dane z pliku\n')

    if opcja == "1":
        sekwencja1 = input('Podaj sekwencje 1: ')
        sekwencja2 = input('Podaj sekwencje 2: ')
    elif opcja == "2":
        plik1 = input('Podaj nazwe pliku 1: ')
        plik2 = input('Podaj nazwe pliku 2: ')

        sekwencja1 = ''
        plik = open(plik1, 'r')
        for x in plik:
            if x.startswith('>'):
                sekwencja1 += x
        sekwencja2 = ''
        plik = open(plik2, 'r')
        for x in plik:
            if x.startswith('>') is False:
                sekwencja2 += x

    okno = input("Podaj rozmiar okna: ")
    prog = input("Podaj wartość progu: ")

    return sekwencja1, sekwencja2, opcja, okno, prog

def czy_poprawne(sekwencja1, sekwencja2,opcja):
    """
        Funkcja umożliwia sprawdzanie poprawności wprowadzanych liter,
        czy należą do liter wykorzystywanych w aminikwasach.
    :param sekwencja1:
    :param sekwencja2:
    """
    if opcja == "1":
        i = 0;
        aminokwasy = ["A", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "Y"]
        while i in range(len(sekwencja1)):
            if sekwencja1[i] not in aminokwasy:
                raise ValueError("Błąd! Niepoprawna sekwencja - 1 sekwencja")
                break
            i+=1

        i = 0;
        aminokwasy = ["A", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "Y"]
        while i in range(len(sekwencja2)):
            if sekwencja2[i] not in aminokwasy:
                raise ValueError("Błąd! Niepoprawna sekwencja - 2 sekwencja")
                break
            i += 1


def macierz(sekwencja1, sekwencja2):
    """
    Funkcja korzysta z wcześniej zadeklarowanej funkcji czy_rowne,
    dodaje do macierzy (tablicy dwuwymiarowej) odpowiednie wartości.

    :param sekwencja1:
    :param sekwencja2:
    Returns:
           tablica1
       """
    tablica1 = []
    i = 0
    while i in range(len(sekwencja1)):
        tablica2 = []
        j = 0
        while j in range(len(sekwencja2)):
            if sekwencja1[i] == sekwencja2[j]:
                tablica2.append(1)
            else:
                tablica2.append(0)
            j += 1
        tablica1.append(tablica2)
        i += 1
    return tablica1

def kopia(sekwencja1, sekwencja2):
    """"
    Funkcja korzysta z wcześniej zadeklarowanej funkcji macierz
    i tworzy kopię powstałej macierzy

    :param sekwencja1:
    :param sekwencja2:
    Returns:
        tablica_kopia
    """
    tablica_dwuwymiarowa = np.array(macierz(sekwencja1, sekwencja2))
    tablica_kopia = tablica_dwuwymiarowa.copy()

    return tablica_kopia

def filtracja(tablica_kopia, sekwencja1, sekwencja2, okno, prog):
    """
    Funkcja korzysta z wcześniej skopiowanej macierzy na której będzą
    wykonywane działania podczas filtracji. Wykorzystuje również
    wpisane wcześniej przez uzytykownika wartości dla okna i progu.
    Funkcja odpowiada za filtracje macierzy.

    :param tablica_kopia:
    :param sekwencja1:
    :param sekwencja2:
    :param okno:
    :param prog:
    Returns:
        tablica_kopia, sekwencja1, sekwencja2
    """
    wiersz = len(sekwencja1)
    kolumna = len(sekwencja2)

    for i in range(-(kolumna - 1), wiersz):
        przesuniecie = 0
        for j in range(len(tablica_kopia.diagonal(i) - 1)):
            if tablica_kopia.diagonal(i)[j:okno + przesuniecie].sum() < prog:
                if i < 0:
                    tablica_kopia[abs(i) + j][j] = 0
                else:
                    tablica_kopia[j][abs(i) + j] = 0
            if i < okno + przesuniecie <= len(tablica_kopia.diagonal(i)) - 1:
                przesuniecie += 1

    return tablica_kopia, sekwencja1, sekwencja2

def wykres(sekwencja1, sekwencja2, opcja):
    """
    Funkcja generuje wykres, opisuje osie danymi użytymi w zadaniu,
    zapisuje wykres w pliku

    :param sekwencja1:
    :param sekwencja2:
    :param opcja:
    Returns:
            obraz
    """
    plt.imshow(np.array(macierz(sekwencja1, sekwencja2)), cmap='Greens')
    if(opcja == "1"):
        plt.xticks(np.arange(len(list(sekwencja1))), list(sekwencja1))
        plt.yticks(np.arange(len(list(sekwencja2))), list(sekwencja2))

    plt.title('Dopasowanie par sekwencji', fontsize=16)
    plt.xlabel("sekwencja 1", fontsize=16)
    plt.ylabel("sekwencja 2", fontsize=16)
    plt.grid(False)
    plt.savefig("bezfiltracji.jpg", dpi=120)
    plt.show()

def wykres_filtracja(tablica_kopia,sekwencja1, sekwencja2, opcja):
    """
    Funkcja generuje wykres, opisuje osie danymi użytymi w zadaniu,
    zapisuje wykres w pliku

    :param tablica_kopia:
    :param sekwencja1:
    :param sekwencja2:
    :param opcja:
    Returns:
            obraz
    """
    plt.imshow(tablica_kopia, cmap='Greens')
    if(opcja == "1"):
        plt.xticks(np.arange(len(list(sekwencja1))), list(sekwencja1))
        plt.yticks(np.arange(len(list(sekwencja2))), list(sekwencja2))

    plt.title('Dopasowanie par sekwencji', fontsize=16)
    plt.xlabel("sekwencja 1", fontsize=16)
    plt.ylabel("sekwencja 2", fontsize=16)
    plt.grid(False)
    plt.savefig("zfiltracja.jpg", dpi=120)
    plt.show()


try:
    sekwencja1, sekwencja2, opcja, okno, prog = odczyt_pliku_fasta()
    czy_poprawne(sekwencja1, sekwencja2, opcja)
    tablica_kopia = kopia(sekwencja1, sekwencja2)

    filtracja(tablica_kopia, sekwencja1, sekwencja2, int(okno), int(prog))
    wykres(sekwencja1, sekwencja2, opcja)
    wykres_filtracja(tablica_kopia,sekwencja1, sekwencja2, opcja)
except ValueError as e:
    print(e)


